<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bbs extends Model
{
    protected $table = 'bbs';

    protected $fillable = ['name', 'title', 'body', 'ip', 'ua'];

    public function insert(){
        $this->name = $_POST['name'];
        $this->title = $_POST['title'];
        $this->body = $_POST['body'];
        $this->ua = $_SERVER['HTTP_USER_AGENT'];
        $this->ip = $_SERVER['REMOTE_ADDR'];

        if(isset($_FILES['file']))
		{
			$this->path = $_FILES['file']['name'];
			move_uploaded_file($_FILES['file']['tmp_name'], public_path("files/" . $_FILES['file']['name']));
		}
		else
		{
			$this->path = '';
		}
        $this->save();
    }

    public static function getTotal(){
        $sql = "SELECT count(id) as c FROM bbs;";
        return \DB::select(\DB::raw($sql))[0]->c;
    }

    public static function getList(){
        $min = 20 * $_GET['p'];
        $sql = "SELECT * FROM bbs ORDER BY id DESC LIMIT {$min}, 20;";
        return \DB::select(\DB::raw($sql));
    }
}
