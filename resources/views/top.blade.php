<!doctype html>
<html lang="ja">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AD {{ __('messages.corporation_type') }}</title>
        <link rel="stylesheet" href="/css/bootstrap.css">
        <link rel="stylesheet" href="/css/main.css">
        <script src="/js/bootstrap.js"></script>
        <script src="/js/jquery.min.js"></script>
        <script src="/js/main.js"></script>
    </head>
    <body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <a class="navbar-brand" href="/"><span class="text-danger">A</span>D {{ __('messages.corporation_type') }}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/news.php">{{ __('messages.title_news') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/bbs">{{ __('messages.title_bbs') }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/company">{{ __('messages.title_company') }}</a>
      </li>
  </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">{{ __('messages.title_news') }}</h1>
            <div class="news">
                <?php
                $dirs = explode("\n", shell_exec("ls news{$_POST['wtf']}"));
                    foreach($dirs as $dir):
                ?>
                <h3 class="text-center"><a href="/news.php?q=<?= $dir; ?>"><?php echo urldecode(trim($dir)); ?></a></h3>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-12">
            <h1 class="text-center">{{ __('messages.top_recent') }}</h1>
            <?php foreach($list as $bbs):?>
                <h3><a href="/bbs"><?= $bbs->title;?></a></h3>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<div class="inquiry">
<h2 class="text-center">{{ __('messages.top_contact') }}</h2>
<form action="" method="post" id="contact">
<div class="form-group">
<label>{{ __('messages.top_name') }}</label>
<input class="form-control" name="name" id="name">
</div>
<div class="form-group">
<label>{{ __('messages.top_email') }}</label>
<input class="form-control" name="email" id="email">
</div>
<div class="form-group">
<label>{{ __('messages.top_type') }}</label>
<select class="form-control" name="type" id="type">
<option value="0">{{ __('messages.top_type_service') }}</option>
<option value="1">{{ __('messages.top_type_recruit') }}</option>
<option value="2">{{ __('messages.top_type_other') }}</option>
</select>
</div>
<div class="form-group">
<label>{{ __('messages.top_content') }}</label>
<textarea class="form-control" name="body" id="body"></textarea>
</div>
<button onclick="send(); return false;" class="btn btn-block btn-default">{{ __('messages.top_sent') }}</button>
</form>
</div>
<footer class="text-center">
Copyright AD Inc. All Rights Reserved.
</footer>
</body>
</html>
