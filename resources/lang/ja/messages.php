<?php

return [

    /*
    |--------------------------------------------------------------------------
    | common(title/nav)
    |--------------------------------------------------------------------------
    */

    'corporation_type' => '株式会社',
    'title_news' => 'お知らせ',
    'title_bbs' => '掲示板',
    'title_company' => '会社概要',

    /*
    |--------------------------------------------------------------------------
    | views/home/index.blade.php
    |--------------------------------------------------------------------------
    */

    'index_menu' => 'Menu',
    'index_news' => 'お知らせ管理',
    'index_contact' => 'お問い合わせ確認',

    /*
    |--------------------------------------------------------------------------
    | views/home/news.blade.php
    |--------------------------------------------------------------------------
    */

    'news_add' => '新規追加',
    'news_title' => 'タイトル',
    'news_content' => '本文',
    'news_regist' => '追加',
    'news_delete' => '削除',


    /*
    |--------------------------------------------------------------------------
    | views/home/top.blade.php
    |--------------------------------------------------------------------------
    */

    'top_recent' => '最近の投稿',
    'top_contact' => 'お問い合わせ',
    'top_name' => 'お名前',
    'top_email' => 'メールアドレス',
    'top_type' => '種類',
    'top_type_service' => 'サービスについて',
    'top_type_recruit' => '採用について',
    'top_type_other' => 'その他',
    'top_content' => '内容',
    'top_sent' => '送信',

    /*
    |--------------------------------------------------------------------------
    | views/home/company.blade.php
    |--------------------------------------------------------------------------
    */

    'company_name_title' => '会社名',
    'company_name' => 'AD株式会社',

    'company_address_title' => '住所',
    'company_address' => '東京都',

    'company_establishment_title' => '設立年月日',
    'company_establishment' => '1981/11/11',

    'company_officer_title' => '役員',
    'company_officer' => '代表取締役　攻防　太郎',

    'company_business_title' => '業務内容',
    'company_business' => 'インターネットを用いたサービス',

    'company_employees_title' => '従業員数',
    'company_employees' => '51',

    'company_bank_title' => '取引銀行',
    'company_bank' => '○○銀行',

    'company_motto_title' => '社訓',
    'company_motto' => '怪物と闘う者は、その過程で自らが怪物と化さぬよう心せよ。おまえが長く深淵を覗くならば、深淵もまた等しくおまえを見返すのだ。',

    /*
    |--------------------------------------------------------------------------
    | views/home/contact.blade.php
    |--------------------------------------------------------------------------
    */

    'contact_type_service' => 'サービスについて',
    'contact_type_recruit' => '採用について',
    'contact_type_other' => 'その他',
    'contact_back' => '戻る',
    'contact_next' => '進む',


    /*
    |--------------------------------------------------------------------------
    | views/home/submit.blade.php
    |--------------------------------------------------------------------------
    */

    'bbs_post' => '掲示板投稿',
    'bbs_discription' => '下記をご記入ください',
    'bbs_name' => 'お名前',
    'bbs_title' => 'タイトル',
    'bbs_file' => 'ファイル',
    'bbs_content' => '内容',
    'bbs_sent' => '送信',

    /*
    |--------------------------------------------------------------------------
    | views/home/list.blade.php
    |--------------------------------------------------------------------------
    */

    'list_to_post' => '投稿画面へ',
    'list_add' => '追加',
    'list_comment_title' => 'コメント',
    'list_comment_post' => 'コメント投稿',
    'list_name' => 'お名前',
    'list_content' => '内容',
    'list_submit' => '送信',
    'list_back' => '戻る',
    'list_next' => '進む',

];

